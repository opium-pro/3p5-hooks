const fs = require('fs')

const swagger = JSON.parse(fs.readFileSync('scripts/swagger.json'))

String.prototype.capitalize = function () {
  return this.charAt(0).toUpperCase() + this.slice(1).toLocaleLowerCase()
}

var targetDir = 'src/pkg/services/.generated/rest'
if (!fs.existsSync(targetDir)){
    fs.mkdirSync(targetDir, { recursive: true });
}

const { paths } = swagger

const files = {}

function makeTypeFromParams(params) {
  if (!params) {return}

  return params.map((item) => {
    if (item.schema?.type === 'array') {
      item.schema.type = `${item.schema.items?.type || any}[]`
    }

    if (item.schema?.type === 'integer') {
      item.schema.type = 'number'
    }

    const required = item.required ? '' : '?'

    return `${item.name}${required}: ${item.schema?.type}`
  })
}


for (const path in paths) {
  const splitOriginalPath = path.split('/')
  const fileName = `${splitOriginalPath[1]}.ts`

  if (!Object.keys(files).includes(fileName)) {
    files[fileName] = []
  }

  for (const method in paths[path]) {
    if (paths[path][method].deprecated) {
      continue
    }

    let functionName = `${method.toLowerCase()}`

    let countPathParams = 0
    const splitPath = splitOriginalPath.map((pathItem, index) => {
      if (index == 0 || index == 1) { return pathItem }

      if (pathItem.indexOf('{') === 0) {
        functionName += 'Item'
        return `\${path[${countPathParams++}]}`
      }

      functionName += pathItem.replace(/[^\w\s]/gi, '').capitalize()
      return pathItem
    })

    const pathParams = paths[path][method].parameters?.filter((item) => item.in === 'path')
    const bodyParams = paths[path][method].parameters?.filter((item) => item.in === 'query')

    const pathParamsType = 'Array<string | number>'
    const bodyParamsType = bodyParams ? `{${makeTypeFromParams(bodyParams).join(', ')}}` : '{[key:string]: string | number}'

    const stubPath = 'src/pkg/stubs/' + splitPath[1] + '/' + functionName + '.ts'
    const stub = fs.existsSync(stubPath) && '../../../stubs/' + splitPath[1] + '/' + functionName

    files[fileName].push({
      name: functionName,
      path: splitPath.join('/'),
      method: method.toUpperCase(),
      pathParamsType,
      bodyParamsType,
      stub,
    })
  }
}

let indexText = ''
for (const fileName in files) {
  const shortName = fileName.split('.')[0]
  indexText += `export * as ${shortName}Services from './${shortName}'
`
  let text = `/* eslint-disable */

import { fetch } from '../../../utils'
`

  files[fileName].forEach(({ name, path, method, pathParamsType, bodyParamsType, stub }) => {
    text += `
${stub ? `
import stub_${name} from '${stub}'` : ''}
type Type_${name}_path = ${pathParamsType} | string | number
type Type_${name}_body = ${bodyParamsType}
export const ${name} = (params?: Type_${name}_path | Type_${name}_body, bodyParams?: Type_${name}_body) => {
  const noPathParams = params instanceof Object
  const data = noPathParams ? params : bodyParams
  const path = noPathParams ? [] : (Array.isArray(params) ? params : [params])
  return fetch({
    url: \`${path}\`,
    method: '${method}',
    data,
  }${stub ? `, {data: stub_${name}}` : ''})
}
`
  })

  fs.writeFileSync(`${targetDir}/${fileName}`, text)
}
fs.writeFileSync(`${targetDir}/index.ts`, indexText)