import { createSelector, useStorage } from 'master-hook'
import { STORAGE_NAME } from './constants'
import { dismissError } from './actions'


export const lastError: any = createSelector(
  () => useStorage(STORAGE_NAME).errors,
  (errors) => {
    return [...errors].pop()
  }
)