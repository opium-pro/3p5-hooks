export const STORAGE_NAME = 'app'

export const initialState = {
  isLocked: false as boolean,
  pincode: undefined as string,
  messages: [] as object[],
  errors: [] as object[],
  useStubs: false as boolean,
  lastActivity: undefined,
}

export const cache = {
  isLocked: 0,
  pincode: 0,
  useStubs: 0,
}