import { STORAGE_NAME } from './constants'
import { useStorage, createAction } from 'master-hook'
import { STORAGE_NAME as authStorageName } from '../use-auth'
import { STORAGE_NAME as userStorageName } from '../use-user'

let errorId = 0
let messageId = 0


export const registerError = createAction(({ title, text, more, code = 2500, source = 'undefined' }, addToStack = true) => {
  const { errors, setErrors } = useStorage(STORAGE_NAME)
  const newErrors = [...errors]
  const error = { id: errorId++, title, text, code, source }
  newErrors.push(error)
  console.error(`
Ошибка ${code}: ${title}
Источник: '${source}'

${text}

More info:
${typeof (more) === 'object' ? JSON.stringify(more) : more}`
  )
  // Не показываем ошибку, если это ошибки клиента (4xx)
  const has4xxError = code && code.toString().length === 3 && code.toString()[0] === '4'
  if (addToStack && !has4xxError) {
    setErrors(newErrors)
  }

  return error
})


export const dismissError = createAction((id) => {
  const { errors, setErrors } = useStorage(STORAGE_NAME)
  const newErrors = errors.filter(item => item.id !== id)
  setErrors(newErrors)
})

// Следим за тем, что люди делают в приложении
// TODO. Нужно сделать периодическую отвправку на сервер
export const registerActivity = createAction((path?: string, ...params: any) => {
  const { setLastActivity } = useStorage(STORAGE_NAME)
  const timestamt = new Date()
  setLastActivity(timestamt)
})

// Дергаем колбэк, если пользователь неактивен дольше необходимого времени
export const trackUnactive = createAction(function track(duration, callback) {
  const then = useStorage(STORAGE_NAME).lastActivity
  setTimeout(() => {
    const last = useStorage(STORAGE_NAME).lastActivity
    if (last === then) {
      callback()
    } else {
      const newDuration = duration - (new Date().getTime() - new Date(last).getTime())
      track(newDuration, callback)
    }
  }, duration)
})


export const purge = createAction(async () => {
  // TODO дописать сюда ресет нескольких сторажей из мастерхука одновременно
  const { reset: resetApp } = useStorage(STORAGE_NAME)
  const { reset: resetAuth } = useStorage(authStorageName)
  const { reset: resetUser } = useStorage(userStorageName)
  resetApp()
  resetAuth()
  resetUser()
}, { setIsPendingTo: [STORAGE_NAME, authStorageName, userStorageName] })