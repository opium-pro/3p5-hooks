export default [
  {
    id: "1",
    name: "United Kingdom",
    abbr: "uk",
    flag: "ссылка на флаг",
    number_of_banks: 2,
    number_of_offers: 3,
    status: "active",
  },

  {
    id: "2",
    name: "Estonia",
    abbr: "es",
    flag: "ссылка на флаг",
    number_of_banks: 2,
    number_of_offers: 3,
    status: "active",
  },

  {
    id: "3",
    name: "Eblania",
    abbr: "eb",
    flag: "ссылка на флаг",
    number_of_banks: 0,
    number_of_offers: 5,
    status: "testing",
  },
]