export default [
  {
    id: "1",
    name: "opium.pro",
    full_name: "opium.pro",
    logo: "ссылка на лого",
    website: "http://opium.pro",
    number_of_offers: 3,
    status: "active",
    tags: "блабла", // теги для поиска
  },

  {
    id: "2",
    name: "3+5",
    full_name: "Three Plus Five",
    logo: "ссылка на лого",
    website: "https://3p5.app",
    number_of_offers: 0,
    status: "active",
    tags: "блабла",
  },

  {
    id: "3",
    name: "JTC",
    full_name: "Just a Team of Creators",
    logo: "ссылка на лого",
    website: "http://jtc.ooo",
    number_of_offers: 0,
    status: "active",
    tags: "блабла",
  },
]