export default [
  {
    id: "1",
    name: "Как правильно разделывать коня",
    categories: ["123", "12312"], // ид категорий
    number_of_likes: 4,
    number_of_shares: 34,
    status: "active",
    slides: [
      {
        title: "Поговорим о женском",
        hint: undefined,
        description: undefined,
        img: "ссылка на картинку",
        background_type: 0, // это тип бэкграунда, у нас их будет несколько,
        link: "ссылка по кнопке",
        call_to_action: undefined,
        use_as_thumbnail: false, // указывает если это тслайд нужно взять для обложки
      },
      {
        title: "Как правильно разделывать коня",
        hint: "советы для любознательных домохозяек",
        description: "Берешь здоровый нож и разделываешь",
        img: "ссылка на картинку",
        background_type: 2,
        link: "ссылка по кнопке",
        call_to_action: "Купить живого коня",
        thumbnail: true,
      }
    ]
  },

  {
    id: "2",
    name: "Как правильно разделывать коня",
    categories: ["123", "12312"], // ид категорий
    number_of_likes: 4,
    number_of_shares: 34,
    status: "drafts",
    slides: [
      {
        title: "Поговорим о женском",
        hint: undefined,
        description: undefined,
        img: "ссылка на картинку",
        background_type: 0, // это тип бэкграунда, у нас их будет несколько,
        link: "ссылка по кнопке",
        call_to_action: undefined,
        use_as_thumbnail: false, // указывает если это тслайд нужно взять для обложки
      },
      {
        title: "Как правильно разделывать коня",
        hint: "советы для любознательных домохозяек",
        description: "Берешь здоровый нож и разделываешь",
        img: "ссылка на картинку",
        background_type: 2,
        link: "ссылка по кнопке",
        call_to_action: "Купить живого коня",
        thumbnail: true,
      }
    ]
  },
]