export default [
  {
    "id": "1001",
    "name": "Allied Irish Bank",
    "logo": "http://files.opium.pro/temp/bank-logos/allied-irish-banks.svg",
    "website": "https://aib.ie/",
    "known": true,
    "status": 'active',
  },
  {
    "id": "1002",
    "name": "Bank of Ireland",
    "logo": "http://files.opium.pro/temp/bank-logo-png/bank-of-ireland.png",
    "website": "https://www.bankofireland.com/",
    "known": true,
    "status": 'active',
  },
  {
    "id": "1003",
    "name": "Bank of Scotland",
    "logo": "http://files.opium.pro/temp/bank-logos/bank-of-scotland.svg",
    "website": "https://www.bankofscotland.co.uk/",
    "loggedIn": true,
    "status": 'active',
  },
  {
    "id": "1004",
    "name": "Barclays Bank",
    "logo": "http://files.opium.pro/temp/bank-logo-png/barklays.png",
    "website": "https://www.barclays.co.uk/",
    "status": 'active',
  },

  {
    "id": "1005",
    "name": "Citibank",
    "logo": "http://files.opium.pro/temp/bank-logos/citibank.svg",
    "website": "https://www.citibank.co.uk",
    "status": 'active',
  },
  {
    "id": "1006",
    "name": "Halifax",
    "logo": "http://files.opium.pro/temp/bank-logos/halifax.svg",
    "website": "https://www.halifax.co.uk/",
    "status": 'active',
  },
  {
    "id": "1007",
    "name": "HSBC",
    "logo": "http://files.opium.pro/temp/bank-logo-png/hsbc.png",
    "website": "https://www.hsbc.co.uk/",
    "status": 'active',
  },
  {
    "id": "1008",
    "name": "Lloyds Bank",
    "logo": "http://files.opium.pro/temp/bank-logo-png/lloyds.png",
    "website": "https://www.lloydsbank.com/",
    "status": 'available',
  },
  {
    "id": "1009",
    "name": "Mizuho Bank",
    "logo": "http://files.opium.pro/temp/bank-logo-png/mizuho-logo.png",
    "website": "https://www.mizuhogroup.com",
    "status": 'available',
  },
  {
    "id": "1010",
    "name": "Revolut",
    "logo": "http://files.opium.pro/temp/bank-logo-png/revolut.png",
    "website": "https://www.revolut.com",
    "status": 'available',
  },
  {
    "id": "1011",
    "name": "Sainsbury’s Bank",
    "logo": "http://files.opium.pro/temp/bank-logo-png/sainsburys-1.png",
    "website": "https://www.sainsburys.co.uk",
    "status": 'testing',
  },
  {
    "id": "1012",
    "name": "Santander",
    "logo": "http://files.opium.pro/temp/bank-logos/Santander.svg",
    "website": "https://www.santander.co.uk",
    "status": 'testing',
  },
  {
    "id": "1013",
    "name": "SG Kleinwort Hambros Bank",
    "logo": "http://files.opium.pro/temp/bank-logos/SG.svg",
    "website": "https://www.kleinworthambros.com/en",
    "status": 'draft',
  },

  {
    "id": "1014",
    "name": "Starling Bank",
    "logo": "http://files.opium.pro/temp/bank-logo-png/starling-bank.png",
    "website": "https://www.starlingbank.com",
    "status": 'draft',
  },
  {
    "id": "1015",
    "name": "Tesco Bank",
    "logo": "http://files.opium.pro/temp/bank-logos/tesco.svg",
    "website": "https://www.tescobank.com",
    "status": 'draft',
  },
]