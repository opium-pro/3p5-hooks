import { AxiosRequestConfig } from 'axios'
import { useStorage } from 'master-hook'
import { registerError } from '../use-app/actions'
import { refreshTokensAndRepeat } from './refresh-tokens-and-repeat'
import { STORAGE_NAME as appStorageName } from '../use-app'
import { DEV_MODE } from '../constants'
import { makeRequest } from './make-request'


/**
 * Fetch data from 3p5 server
 * @param options HTTP request options
 * @param stub Fake responce
 * @returns `undefined` if error, responce body if success
 */

export const fetch = async (options: AxiosRequestConfig, stub?: Stub) => {
  if (!options) { return }

  /* Helpers */
  function errorHandler(error, title?: string, code?: number) {
    registerError({
      code: code || error?.response?.status + 1000,
      title: `Ошибка запроса: ${title || error?.message}`,
      text: `Запрос: ${JSON.stringify(options)}
Ответ: ${JSON.stringify(error?.response)}`,
      more: options,
      source: '3p5-hooks/utils/fetch',
    })
  }

  // Return stubs if needed
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { useStubs } = useStorage(appStorageName)
  const applyStubs = DEV_MODE && useStubs && stub && !stub.isDebugOff === true
  if (applyStubs) {
    const delay = stub?.time || 500
    return stub?.result === "ERROR"
      ? setTimeout(() => errorHandler(stub?.error), delay)
      : setTimeout(() => stub?.data, delay)
  }

  // Making request
  return makeRequest(options, undefined).catch((error) =>
    [401, 403].includes(error.response?.status)
      ? refreshTokensAndRepeat(options)
      : errorHandler(error)
  ) as any
}


/* Types */
interface Stub {
  result?: "ERROR" | "SUCCESS",
  error?: any,
  data: any,
  time?: number,
  isDebugOff?: boolean,
}