import { useStorage } from 'master-hook'
import { registerError } from '../use-app/actions'
import { STORAGE_NAME as authStorageName } from '../use-auth'
import { purge } from '../use-app/actions'
import { makeRequest } from './make-request'

/**
 * Attempt to refresh tokens and repeat initial request
 * @param options Initial HTTP request options
 * @returns `undefined` if error, responce body if success
 */


let lastTimeRequested

export async function refreshTokensAndRepeat(options) {
  /* Helpers */
  function errorHandler(error, title?: string, code?: number) {
    registerError({
      code: code || error?.response?.status,
      title: `Повторный запрос на сервер не сработал: ${title || error?.message}`,
      text: error,
      more: options,
      source: '3p5-hooks/utils/refresh-tokens-and-repeat',
    })
  }

  /* Start */
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { refreshToken, setRefreshToken, setAccessToken } = useStorage(authStorageName)

  // Check existing refresh token
  if (!refreshToken) {
    purge()
    return errorHandler(null, `Отсутствует 'refresh_token'`, 2401)
  }

  // Request new tokens
  const { access_token, refresh_token } = makeRequest({
    url: `/user/sessions/refresh`,
    method: "POST",
    data: { refresh_token: refreshToken },
  }).catch(errorHandler as any) as any

  // Check new access token
  if (!access_token) {
    purge()
    return errorHandler(null, `Не получен 'access_token'`, 2401)
  }

  // Set tokens to storage
  await setRefreshToken(refresh_token)
  await setAccessToken(access_token)

  // Reject if requests are too often
  if (lastTimeRequested < 1000 * 60) {
    return errorHandler(null, 'Слишком частые запросы на обновление токена', 2429)
  }

  // Repeat initial request
  lastTimeRequested = new Date().getTime()
  return makeRequest(options, access_token).catch(errorHandler as any) as any
}