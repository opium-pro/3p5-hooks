import axios from 'axios'
import { useStorage } from 'master-hook'
import { API_URL } from '../constants'
import { STORAGE_NAME as authStorageName } from '../use-auth'

export function makeRequest(request: any, access_token?: string) {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const accessToken = access_token || useStorage(authStorageName).accessToken

  const headers = { ...request.headers }
  if (accessToken) {
    headers.Authorization = `Bearer ${accessToken}`
  }

  const url = `${API_URL}${request.url}`
  const newRequest = { ...request, url, headers }

  return axios(newRequest).then(({ data }) => data)
}