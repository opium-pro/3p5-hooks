import { fetch } from '../utils'

interface MetaData {
  [x: string]: any
}

const getModels = (collection: string, initialName?: string) => {
  const name = initialName || collection.charAt(0).toUpperCase() + collection.slice(1)

  return ({
    [`post${name}`]: async (data: MetaData = {}): Promise<string> => {
      if (!data.status) {
        data.status = 'drafts'
      }

      return fetch({
        url: `/fire123base`,
        method: 'POST',
        data: {
          collection,
          data,
          action: 'add'
        },
      })
    },

    [`patch${name}Item`]: async (doc_id: string, data: MetaData): Promise<void> => {
      return fetch({
        url: `/fire123base`,
        method: 'POST',
        data: {
          collection,
          data,
          doc_id,
          action: 'set'
        },
      })
    },

    [`get${name}`]: async () => {
      return fetch({
        url: `/fire123base`,
        method: 'POST',
        data: {
          collection,
          action: 'get_all'
        },
      })
    },

    // [`get${name}Item`]: async (doc_id: string) => {
    //   return fetch({
    //     url: `/fire123base`,
    //     method: 'POST',
    //     data: {
    //       collection,
    //       doc_id,
    //       action: 'get'
    //     },
    //   })
    // },

    [`delete${name}Item`]: async (doc_id: string) => {
      return fetch({
        url: `/fire123base`,
        method: 'POST',
        data: {
          collection,
          doc_id,
          action: 'delete'
        },
      })
    }
  })
}


export const brandsServices = getModels('brands')
export const admitadServices = getModels('cpa/admitad/offers', 'AdmitadOffers')
export const adminOffersServices = getModels('offers')
