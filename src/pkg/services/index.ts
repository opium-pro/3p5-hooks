import { adminServices, userServices, publicServices, systemServices } from './.generated/rest'
import { admitadServices, adminOffersServices } from './fb'
import { Client, createClient, everything } from './.generated/gql'
import { fetch } from '../utils'
// import { adminManualServices } from './manual'

const newAdminServices = {...adminServices}

Object.keys(admitadServices).forEach((key) => {
  newAdminServices[key] = admitadServices[key]
})

Object.keys(adminOffersServices).forEach((key) => {
  newAdminServices[key] = adminOffersServices[key]
})

export * from './firebase'
export { newAdminServices as adminServices, userServices, publicServices, systemServices }

const getCrud = (name: string) => ({
  findUnique: `findUnique${name}`,
  findFirst: `findFirst${name}`,
  findMany: `findMany${name}`,
  create: `create${name}`,
  createMany: `createMany${name}`,
  update: `update${name}`,
  updateMany: `updateMany${name}`,
  delete: `delete${name}`,
  deleteMany: `deleteMany${name}`,
  upsert: `upsert${name}`,
  aggregate: `aggregate${name}`,
  groupBy: `groupBy${name}`
}) as const;

const set = (data?: any) => {
  if (data && data instanceof Object) {
    const result: {
      [key: string]: {
        set: any
      }
    } = {};
    Object.keys(data).forEach((k: string) => {
      result[k] = {
        set: data[k]
      }
    })

    return result;
  }

  return data;
}

export const client = createClient({
  fetcher: (data) => {
      return fetch({
        url: '/graphql',
        method: 'POST',
        data
      })
  },
}) as Client & {all: typeof everything, crud: typeof getCrud, set: typeof set};

client.all = everything;
client.crud = getCrud;
client.set = set;