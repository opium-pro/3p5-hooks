import { fetch } from '../utils'

interface MetaData {
    [x: string]: any
}

const getModels = (collection: string) => ({
    add: async (data: MetaData): Promise<string> => {
        return fetch({
            url: `/fire123base`,
            method: 'POST',
            data: {
                collection,
                data,
                action: 'add'
            },
        });
    },
    set: async (data: MetaData, doc_id?: string): Promise<void> => {
        return fetch({
            url: `/fire123base`,
            method: 'POST',
            data: {
                collection,
                data,
                doc_id,
                action: 'set'
            },
        });
    },
    getAll: async () => {
        return fetch({
            url: `/fire123base`,
            method: 'POST',
            data: {
                collection,
                action: 'get_all'
            },
        });
    },
    get: async (doc_id: string) => {
        return fetch({
            url: `/fire123base`,
            method: 'POST',
            data: {
                collection,
                doc_id,
                action: 'get'
            },
        });
    },
    delete: async (doc_id: string) => {
        return fetch({
            url: `/fire123base`,
            method: 'POST',
            data: {
                collection,
                doc_id,
                action: 'delete'
            },
        });
    }
});

export const firebase = {
    i18n: getModels('i18n')
};
