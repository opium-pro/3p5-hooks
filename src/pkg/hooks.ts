import MasterHook from 'master-hook'
import * as App from "./use-app"
import * as Auth from "./use-auth"
import * as User from "./use-user"
import * as I18n from "./use-i18n"
import { makeHook } from './default-hook'
import AsyncStorage from '@react-native-async-storage/async-storage'

MasterHook.useDevTools(process.env.NODE_ENV === 'development')
MasterHook.setLocalStorage(AsyncStorage)

export const useApp = MasterHook(App)
export const useAuth = MasterHook(Auth)
export const useUser = MasterHook(User)
export const useI18n = MasterHook(I18n)

// Это новые хуки, которые не привязаны к ролям на уровне названий
export const useOffers = MasterHook(makeHook('public', 'Offers', {
  req: (r, c, o) => ({
    [c.findMany]: {
      ...r[c.findMany],
      categories: { id: true, name: true, icon: true },
      brands: { id: true, name: true, logo: true },
    },
    [c.findUnique]: {
      ...r[c.findUnique],
      categories: { id: true, name: true, icon: true },
      brands: { id: true, name: true, logo: true, full_name: true, image: true, website: true },
    },
    [c.update]: {
      ...r[c.update],
      data: {
        ...r[c.update]?.data,
        categories: { connect: r[c.update]?.data.categories.map(id => {id}) },
      }
    }
  })
}))



// Эти хуки нужно будет выпилить со временем

export const useAdminBanks = MasterHook(makeHook('admin', 'Banks'))
export const useAdminOffers = MasterHook(makeHook('admin', 'Offers'))
export const useAdminBrands = MasterHook(makeHook('admin', 'Brands'))
export const useAdminStories = MasterHook(makeHook('admin', 'Stories'))
export const useAdminDocuments = MasterHook(makeHook('admin', 'Docs'))
export const useAdminCountries = MasterHook(makeHook('admin', 'Countries'))
export const useAdminCategories = MasterHook(makeHook('admin', 'Categories'))
export const useAdminUsers = MasterHook(makeHook('admin', 'Users'))
export const useAdminAdmins = MasterHook(makeHook('admin', 'Admins'))

export const usePublicBanks = MasterHook(makeHook('public', 'Banks'))
export const usePublicOffers = MasterHook(makeHook('public', 'Offers'))
export const usePublicDocuments = MasterHook(makeHook('public', 'Docs'))
export const usePublicCountries = MasterHook(makeHook('public', 'Countries'))
export const usePublicCategories = MasterHook(makeHook('public', 'Categories'))

export const useUserBanks = MasterHook(makeHook('user', 'Banks'))
export const useUserStories = MasterHook(makeHook('user', 'Stories'))
export const useUserAccounts = MasterHook(makeHook('user', 'Accounts'))
export const useUserTransactions = MasterHook(makeHook('user', 'Transactions'))
