import { createAction, useStorage } from 'master-hook'
import {STORAGE_NAME} from './constants'
import {firebase} from "../services"


export const loadI18n = createAction(async () => {
  const {setData} = useStorage(STORAGE_NAME)
  const documents = await firebase.i18n.getAll();

  setData(documents);
});
