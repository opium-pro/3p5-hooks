import { createSelector, useStorage } from 'master-hook'
import { STORAGE_NAME, Item } from './constants'

export const t: any = createSelector(
  () => useStorage(STORAGE_NAME).currentLang,
  () => useStorage(STORAGE_NAME).data,
  (currentLang, data = []) => (inputKey: string = '', params?: {[key: string]: string}) => {
    const key = inputKey
      .toLowerCase()
      .replace(/[^a-z{.*}\s]+/ig, '')   // Убираем все лишние символы, оставляем только a-z и переменные типа {var}
      .replace(/\s+/g, ' ')             // Режем двойные пробелы
      .trim()                           // Режем пробелы по краям
      .replace(/[\s]/g, '_');           // Заменяем пробелы на _

    const currentValue = data.find((item: Item) => item.lang === currentLang && item.key === key)?.value;

    if (params) {
      let replacedValue = `${currentValue || inputKey}`;
      Object.keys(params).forEach(k => {
        replacedValue = replacedValue.replace(`{${k}}`, params[k])
      })

      return replacedValue;
    }

    return currentValue || inputKey;
  }
);
