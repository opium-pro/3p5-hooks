export const STORAGE_NAME = 'i18n'

export interface Item {
  id: string,
  key: string,
  value: string,
  lang: string,
  timestamp: string
}

export interface IState {
  data?: Array<Item>,
  currentLang: string
}

export const initialState: IState = {
  data: undefined,
  currentLang: 'en'
}

export const cache = {
  data: 0,
  currentLang: 0
}
