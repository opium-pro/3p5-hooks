export const DEV_MODE = process.env.NODE_ENV === 'development'
export const API_URL = process.env.REACT_APP_API_URL || 'https://api.3p5.app'