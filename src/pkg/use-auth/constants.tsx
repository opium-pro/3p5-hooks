export const STORAGE_NAME = 'auth'


export const initialState = {
  isLoggedIn: false as boolean,
  accessToken: undefined as string,
  refreshToken: undefined as string,
  sessionKey: undefined as string,
  device: undefined as object,
}

export const cache = {
  isLoggedIn: 0,
  accessToken: 0,
  refreshToken: 0,
  sessionKey: 0,
  device: 0,
}