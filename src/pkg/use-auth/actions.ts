import { userServices, adminServices } from "../services"
// import * as Device from "expo-device";
// import * as AuthSession from "expo-auth-session"
// import * as Localization from "expo-localization"
import { createAction, useStorage } from 'master-hook'
import { STORAGE_NAME } from './constants'
import { purge } from '../use-app/actions'


export const getCurrentSession = createAction(async () => {
  const { sessionKey, setSessionKey, setDevice } = useStorage(STORAGE_NAME)
  if (sessionKey) { return sessionKey }
  const device = {
    // device_model: `${Device.brand} ${Device.modelName}`,
    // system_version: `${Device.osName} ${Device.osVersion}`,
    application_name: "3+5 Android App v1.11",
    // ip: `${Localization.timezone}`,
    // system_language_code: Localization.locale,
  };
  const { session_key } = await userServices.postSessions(device)
  setSessionKey(session_key)
  setDevice(device)
  return session_key
})


export const logOut = createAction(async () => {
  // Выкидываем из приложения, затираем данные
  purge()
  userServices.deleteSessions({ session_id: "current" })
}, { setIsPendingTo: STORAGE_NAME })


// Чтобы не дублировать в каждом этот код
// Так как у нас несколько способов логина
export const registerLogin = createAction(async (args) => {
  // Проверяем, пришли ли токены
  if (!args || !args.access_token || !args.refresh_token) { return }

  const { setIsLoggedIn, setAccessToken, setRefreshToken } = useStorage(STORAGE_NAME)
  setAccessToken(args.access_token)
  setRefreshToken(args.refresh_token)
  setIsLoggedIn(true)
})


// Прямой вход в банк по логину и паролю
export const directBankLogin = createAction(async ({ bank_id, username, password }) => {
  const session_key = await getCurrentSession();
  userServices.postAuthBankDirect({
    session_key,
    bank_id,
    username,
    password,
  }).then((serverResponce) => {
    registerLogin(serverResponce)
  })
}, { setIsPendingTo: STORAGE_NAME })


export const adminLogin = createAction(async ({ email, password }) => {
  const serverResponce = await adminServices.postLogin({
    email,
    password,
  })
  registerLogin(serverResponce)
}, { setIsPendingTo: STORAGE_NAME })


// Быстрый логин ДЛЯ ТЕСТОВ
export const demoBankLogin = createAction(async () => {
  directBankLogin({
    bank_id: "obp-banky-n",
    username: "rob.us.29@example.com",
    password: "a50eda",
  })
})
export const demoAdminLogin = createAction(async () => {
  adminLogin({
    email: "aa@aa.aa",
    password: "aaaaaaa",
  })
})


// Получить проверочный код по смс или воцап
export const sendPhoneCode = createAction(async ({ phone_number, method = "sms" }) => {
  const session_key = await getCurrentSession()
  await userServices.getAuthCode({ session_key, phone_number, method });
})


// Залогиниться по телефону
export const phoneLogin = createAction(async ({ code }) => {
  const session_key = await getCurrentSession()
  const phoneLoginResponce = await userServices.postAuthCode({
    session_key,
    code,
  })
  registerLogin(phoneLoginResponce)
}, { setIsPendingTo: STORAGE_NAME })


// TODO Сделать, чтобы это работало и на мобиле и в вебе
// TODO Добавить пуш на случай ошибок логина / неудачной авторизации
export const oAuthBankLogin = createAction(async (bankId: number) => {
  // const session_key = await getCurrentSession()
  // const redirect_url = AuthSession.makeRedirectUri({useProxy: false})
  // const authUrl = await authService.oAuthBankLogin(
  //   bankId,
  //   session_key,
  //   `${redirect_url}/`,
  // )
  // const request = new AuthSession.AuthRequest({
  //   clientId: session_key,
  //   redirectUri: redirect_url,
  // })
  // const authResult: any = await request.promptAsync(
  //   {},
  //   { useProxy: false, url: authUrl, showInRecents: true }
  // )
  // authResult?.params && registerLogin(authResult.params)
})