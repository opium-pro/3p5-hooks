export function makeState(name?: string, storageName?: string) {
  let singularlName = name
  if (name.slice(-1) === 's') {
    singularlName = name.slice(0, -1)
    if (singularlName.slice(-2) === 'ie') {
      singularlName = singularlName.slice(0, -2) + 'y'
    }
  }

  const initialState = {
    oneItemId: undefined,
    allItems: undefined,
    counters: undefined,
    filter: undefined,
    filterHistory: [],
    isLoaded: false,
    cached: undefined,
    meta: {
      pluralName: name,
      singularName: singularlName,
      rootPath: `/${name.toLowerCase()}`,
      itemPath: `/${name.toLowerCase()}/:id`,
      storageName,
    },
  }

  const cache = {
    allItems: 0,
    counters: 0,
    filterHistory: 0,
    filter: 0,
    meta: 0,
    cached: 0,
  }

  return {initialState, cache}
}