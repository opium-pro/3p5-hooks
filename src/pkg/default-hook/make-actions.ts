import filter from 'opium-filter'
import { useStorage, createAction } from 'master-hook'
import { STORAGE_NAME as appStogareName } from '../use-app/constants'
import * as services from '../services'

export const makeActions = (storageName, path, name) => {
  const actions: any = {}
  const targetServices = {
    getList: services[`${path}Services`][`get${name}`],
    getItem: services[`${path}Services`][`get${name}Item`],
    deleteItem: services[`${path}Services`][`delete${name}Item`],
    patchItem: services[`${path}Services`][`patch${name}Item`],
    postItem: services[`${path}Services`][`post${name}`],
    getCounters: services[`${path}Services`][`get${name}Counters`],
  }

  const {
    getList, deleteItem, getItem, patchItem, getCounters, postItem
  } = targetServices;


  // Load counters
  actions.loadCounters = createAction(async () => {
    const { allItems, setCounters, filter: filterSet } = useStorage(storageName)
    if (getCounters) {
      const counters = await getCounters()
      counters && setCounters(counters)
      return counters
    }

    const statuses = {
      all: allItems?.length,
      filtered: filterSet ? filter(allItems, filterSet)?.length : undefined,
      other: allItems?.filter(item => !item.status).length
    }
    if (allItems) {
      for (const { status } of allItems) {
        if (!!status) {
          statuses[status] = allItems.filter(item => item.status === status)?.length
        }
      }
    }
    setCounters(statuses)
    return statuses
  })


  // Load items
  actions.loadItems = createAction(async () => {
    const { setAllItems, setIsLoaded } = useStorage(storageName)
    if (!getList) { return }
    const newAllItems = await getList()
    await setAllItems(newAllItems)
    await actions.loadCounters()
    if (newAllItems) {
      setIsLoaded(true)
    } else {
      setIsLoaded(false)
    }
    return newAllItems
  }, [storageName])


  // Load one item
  actions.loadOneItem = createAction(async (id?: string | number) => {
    const { oneItemId, allItems, setAllItems } = useStorage(storageName)
    if (!id) { id = oneItemId }
    if (!getItem && getList) {
      const newAllItems = await actions.loadItems()
      const oneItem = newAllItems.find(item => item.id === id)
      return oneItem
    }
    const oneItem = await getItem?.(id)
    if (oneItem) {
      const contains = !!allItems.find(item => item.id === id)
      const newAllItems = contains
        ? [...allItems].map(item => item.id === id ? oneItem : item)
        : [...allItems, oneItem]
      if (!contains) {
        updateCounters([oneItem.status, 1], ['all', 1])
      }
      setAllItems(newAllItems)
    }
    return oneItem
  }, [storageName])


  // Patch one item
  actions.patchOneItem = createAction(async (id?: any, data?: any) => {
    const { allItems, setAllItems, oneItemId } = useStorage(storageName)
    if (!data && id instanceof Object) {
      data = id
      id = oneItemId
    }
    const result = await patchItem?.(id, data)
    let newOneItem
    if (result !== undefined) {
      newOneItem = { ...allItems.find(item => item.id === id), ...data }
    }

    if (newOneItem) {
      const oldOneItem = allItems.find(item => item.id === id)
      let newAllItems = [...allItems]
      if (oldOneItem) {
        newAllItems = allItems.map((item) => item.id === id ? newOneItem : item)
        if (oldOneItem.status !== newOneItem.status) {
          updateCounters([oldOneItem.status, -1], [newOneItem.status, 1])
        }
      } else {
        newAllItems.push(newOneItem)
        updateCounters([newOneItem.status, 1])
      }
      setAllItems(newAllItems)
    }
    return newOneItem
  }, [storageName])


  // Delete one item
  actions.deleteOneItem = createAction(async (id?: any) => {
    const { setOneItemId, oneItemId, allItems, setAllItems } = useStorage(storageName)
    if (!id) { id = oneItemId }
    const result = await deleteItem?.(id)
    if (result !== undefined) {
      const oneItem = allItems.find(item => item.id === id)
      setAllItems(allItems.filter((item) => item.id !== id))
      if (id === oneItemId) { setOneItemId(undefined) }
      updateCounters([oneItem.status, -1], ['all', -1])
    }
    return !!result
  }, [storageName])


  // Add one item
  actions.addOneItem = createAction(async (data) => {
    const { allItems, setAllItems } = useStorage(storageName)
    const newItem = await postItem(data)
    if (newItem) {
      const newAllItems = [newItem, ...allItems]
      setAllItems(newAllItems)
      updateCounters([newItem.status, 1], ['all', 1])
    }
    return newItem
  }, [storageName])


  function updateCounters(...params: [status: string, value: number][]) {
    const { counters, setCounters } = useStorage(storageName)
    const newCounters = { ...counters }
    for (const paramSet of params) {
      const status = paramSet[0]
      const value = paramSet[1]
      if (typeof counters[status] !== 'number') {
        newCounters[status] = 0
      }
      newCounters[status] = newCounters[status] + value
    }
    setCounters(newCounters)
  }


  actions.setFilterHistory = createAction((value) => {
    const { filterHistory, setFilterHistory } = useStorage(storageName)
    if (value && !filterHistory.includes(value) && value.length >= 3) {
      setFilterHistory([value, ...filterHistory])
    }
  })


  return actions
}