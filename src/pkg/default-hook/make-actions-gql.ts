import { useStorage, createAction } from 'master-hook'
import { client } from '../services'

type ValueOf<T> = T[keyof T];
type TRequestParams = {[key in ValueOf<ReturnType<typeof client.crud>>]: any};
type TOpeartions = {
  all: typeof client.all
  set: typeof client.set
}
export type TRequest = (r: TRequestParams, c: ReturnType<typeof client.crud>, o: TOpeartions) => TRequestParams;


export const makeActionsGql = (storageName: string, name: string, request: TRequest = r => r) => {
  const c = client.crud(name);
  const o = {
    all: client.all,
    set: client.set,
    add: (r: TRequest) => r
  }
  const req = (r: TRequestParams) => {
    const params = {
      ...r,
      ...request(r, c, o)
    };

    const removedKeys = Object.keys(params).filter(k => r[k] === undefined);
    removedKeys.forEach(k => {
      delete params[k];
    });

    return params;
  };

  const getList = () => {
    return client.query(req({
      [c.findMany]: client.all,
      [c.groupBy]: [{by: ['status']}, {status: true, _count: {status: true}}],
      [c.aggregate]: {_count: {status: true}}
    }))
  }
  const getItem = (id: number | string) => {
    return client.query(req({
      [c.findUnique]: [{where: {id: Number(id)}}, client.all]
    }))
  }
  const postItem = (data: any) => {
    return client.mutation(req({
      [c.create]: [{data: data}, client.all]
    }))
  }
  const patchItem = (id: string | number, data: any) => {
    return client.mutation(req({
      [c.update]: [{where: {id: Number(id)}, data: client.set(data)}, client.all]
    }))
  }
  const deleteItem = (id: string | number) => {
    return client.mutation(req({
      [c.delete]: [{where: {id: Number(id)}}, client.all]
    }))
  }
  const actions: any = {}

  // Load items
  actions.loadItems = createAction(async () => {
    const { setAllItems, setIsLoaded, setCounters } = useStorage(storageName)
    if (!getList) { return undefined }
    const newAllItems = await getList()

    const statuses = {
      all: newAllItems[c.aggregate] ? newAllItems[c.aggregate]._count._all : 0,
      other: newAllItems[c.findMany]?.filter(item => !item.status).length
    }

    if (newAllItems[c.groupBy]) {
      newAllItems[c.groupBy].forEach(data => {
        statuses[data.status] = data._count.status
      });
    }

    setCounters(statuses)
    await setAllItems(newAllItems[`findMany${name}`])

    if (newAllItems) {
      setIsLoaded(true)
    } else {
      setIsLoaded(false)
    }
    return newAllItems
  }, [storageName])


  // Load one item
  actions.loadOneItem = createAction(async (id?: string | number) => {
    const { oneItemId, allItems = [], setAllItems } = useStorage(storageName)
    if (!id) { id = oneItemId }

    const oneItem = await (await getItem(id))?.[c.findUnique]
    if (oneItem) {
      const contains = !!allItems.find(item => item.id === id)
      const newAllItems = contains
        ? [...allItems].map(item => item.id === id ? oneItem : item)
        : [...allItems, oneItem]
      if (!contains) {
        updateCounters([oneItem.status, 1], ['all', 1])
      }
      setAllItems(newAllItems)
    }
    return oneItem
  }, [storageName])


  // Patch one item
  actions.patchOneItem = createAction(async (id?: any, data?: any) => {
    const { allItems, setAllItems, oneItemId } = useStorage(storageName)
    if (!data && id instanceof Object) {
      data = id
      id = oneItemId
    }
    if (id) {
      id = Number(id)
    }
    const result = await (await patchItem(id, data))?.[c.update]
    let newOneItem
    if (result !== undefined) {
      newOneItem = { ...allItems.find(item => item.id === id), ...data }
    }

    if (newOneItem) {
      const oldOneItem = allItems.find(item => item.id === id)
      let newAllItems = [...allItems]
      if (oldOneItem) {
        newAllItems = allItems.map((item) => item.id === id ? newOneItem : item)
        if (oldOneItem.status !== newOneItem.status) {
          updateCounters([oldOneItem.status, -1], [newOneItem.status, 1])
        }
      } else {
        newAllItems.push(newOneItem)
        updateCounters([newOneItem.status, 1])
      }

      setAllItems(newAllItems)
    }
    return newOneItem
  }, [storageName])


  // Delete one item
  actions.deleteOneItem = createAction(async (id?: any) => {
    const { setOneItemId, oneItemId, allItems, setAllItems } = useStorage(storageName)
    if (!id) { id = oneItemId }
    id = Number(id)
    const result = await (await deleteItem(id))?.[c.delete]
    if (result !== undefined) {
      const oneItem = allItems.find(item => item.id === id)
      setAllItems(allItems.filter((item) => item.id !== id))
      if (id === oneItemId) { setOneItemId(undefined) }
      updateCounters([oneItem.status, -1], ['all', -1])
    }
    return !!result
  }, [storageName])

  // Add one item
  actions.addOneItem = createAction(async (data: any = {}) => {
    const { allItems, setAllItems } = useStorage(storageName)

    const newItem = await (await postItem({...data, status: data.status || 'drafts'}))?.[c.create]
    if (newItem) {
      const newAllItems = [newItem, ...allItems]
      setAllItems(newAllItems)
      updateCounters([newItem.status, 1], ['all', 1])
    }
    return newItem
  }, [storageName])


  function updateCounters(...params: [status: string, value: number][]) {
    const { counters, setCounters } = useStorage(storageName)
    const newCounters = { ...counters }
    for (const paramSet of params) {
      const status = paramSet[0]
      const value = paramSet[1]
      if (typeof counters[status] !== 'number') {
        newCounters[status] = 0
      }
      newCounters[status] = newCounters[status] + value
    }
    setCounters(newCounters)
  }


  actions.setFilterHistory = createAction((value) => {
    const { filterHistory, setFilterHistory } = useStorage(storageName)
    if (value && !filterHistory.includes(value) && value.length >= 3) {
      setFilterHistory([value, ...filterHistory])
    }
  })


  return actions
}