import { makeActions } from './make-actions'
import { makeActionsGql, TRequest } from './make-actions-gql'
import { makeSelectors } from './make-selectors'
import { makeState } from './make-state'

type TOptions = {
  type?: 'gql' | 'rest',
  req?: TRequest
}

export function makeHook(path: string, name: string, options: TOptions = {}) {
  const storageName = `${path ? (path + '-') : ''}${name}`
  const { type = 'gql', req } = options;

  return {
    storage: storageName,
    actions: type === 'gql' ? makeActionsGql(storageName, name, req) : makeActions(storageName, path, name),
    selectors: makeSelectors(storageName),
    ...makeState(name, storageName)
  }
}
