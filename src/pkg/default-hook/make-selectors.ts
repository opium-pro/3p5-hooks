import filter from 'opium-filter'
import {useStorage, createSelector} from 'master-hook'


export const makeSelectors = (storageName) => {
    const selectors: any = {}

    selectors.filteredItems = createSelector(
        () => useStorage(storageName).allItems,
        () => useStorage(storageName).filter,

        (allItems, filterParams) => {
            let result = allItems

            if (typeof filterParams === 'string') {
                result = filter(result, filterParams, {deep: true})
            }

            if (filterParams?.status) {
                result = filter(result, {status: filterParams.status})
            }

            return result
        }
    )


    selectors.oneItem = createSelector(
        () => useStorage(storageName).allItems,
        () => useStorage(storageName).oneItemId,

    (allItems, id) => {
      return filter(allItems, { id: [Number(id)] }, { deep: false })?.[0]
    }
  )

    selectors.nexItem = createSelector(
        () => useStorage(storageName).allItems,
        () => useStorage(storageName).oneItemId,

        (allItems, id) => {
            if (!allItems||!id) {
                return null
            }
            const currentIndex = allItems.findIndex((item) => {
                return item.id === id
            })

            return currentIndex < allItems.length ? allItems[currentIndex + 1] : null
        }
    )

    selectors.prevItem = createSelector(
        () => useStorage(storageName).allItems,
        () => useStorage(storageName).oneItemId,

        (allItems, id) => {
            if (!allItems||!id) {
                return null
            }
            const currentIndex = allItems.findIndex((item) => {
                return item.id === id
            })

            return currentIndex > 0 ? allItems[currentIndex - 1] : null
        }
    )

    return selectors
}