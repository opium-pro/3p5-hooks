import React, { useState } from 'react'
import { Align, Font, Gap, Fit, Box } from 'themeor'
import { SideMenu, TextInput, IconButton } from 'opium-ui'
import * as Package from '../pkg'
import { useNav } from 'opium-nav'
import filter from 'opium-filter'


export const Page = ({ name }) => {
  const [search, setSearch] = useState('')

  const nav = useNav()
  const hook = typeof Package[name] === 'function' ? Package[name]() : Package[name]

  const functions = {}
  const values = {}

  for (const key in hook) {
    if (typeof hook[key] === 'function') {
      functions[key] = hook[key]
    } else {
      values[key] = hook[key]
    }
  }

  function handleFunction(event, key) {
    event.preventDefault()
    if (functions[key] instanceof Function) {
      functions[key]()
    }
  }

  function handleWithParams(event, key) {
    event.preventDefault()
    event.stopPropagation()
    if (functions[key] instanceof Function) {
      functions[key](eval(prompt('Аргументы через запятую')))
    }
  }

  return (<>
    <Fit.TryTagless scroll height="100vh">
    <Box fill="faint-down">
      <Gap size="20px" top="none">
        <Font size="lg" weight="700">{name}</Font>
        <Gap />
        <TextInput label="Search" value={search} onChange={setSearch} />
        <Gap />
        <SideMenu>
          {filter(Object.keys(functions), search).map((key) => (
            <SideMenu.Item
              row
              key={key}
              label={key}
              onClick={(e) => handleFunction(e, key)}
            >
              <Gap stretch />
              <div>
              <IconButton
                tooltip="Вызвать с параметрами"
                onClick={(e) => handleWithParams(e, key)}
                name="input_text"
                fill="faint-down"
              />
              </div>
            </SideMenu.Item>
          ))}
        </SideMenu>
      </Gap>
      </Box>
    </Fit.TryTagless>

    <Box.TryTagless fill="base">
      <Fit scroll height="100vh">
        <Gap size="20px">
          <Align gapVert="20px">
            {Object.keys(values).map((key) => (
              <Align key={key} gapVert="xs">
                <Font weight="700">{key}</Font>
                <Font wrap fill="faint">
                  {JSON.stringify(values[key]) || `${values[key]}`}
                </Font>
              </Align>
            ))}
          </Align>
        </Gap>
      </Fit>
    </Box.TryTagless>
  </>)
}
