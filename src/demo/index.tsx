import React, { useState } from 'react'
import { AppLayout, SideMenu, TextInput } from 'opium-ui'
import * as hooks from '../pkg/hooks'
import { Router, Path, useNav } from 'opium-nav'
import { Page } from './page'
import { Main } from './main'
import { Provider } from './provider'
import filter from 'opium-filter'
import { Gap, Font, Align, Fit } from 'themeor'


const pages = Object.keys(hooks)


export const App = () => {

  return (
    <Provider>
      <AppLayout>
        <Fit.TryTagless cover="screen">
          <Align pattern="200px 300px auto">
            <SideNav />
            <Path name="/" component={Main} />
            {pages.map((page) => (
              <Path
                key={page}
                name={`/${page}`}
                component={(props) => <Page {...props} name={page} />}
              />
            ))}
          </Align>
        </Fit.TryTagless>
      </AppLayout>
    </Provider>
  )
}


function SideNav() {
  const nav = useNav()
  const [search, setSearch] = useState('')

  return (
    <Fit scroll maxHeight="100vh">
      <SideMenu width="200px" minHeight="100vh">
        <Gap>
          <Font size="lg" weight="700">Hooks</Font>
          <Gap />
          <TextInput label="Search" value={search} onChange={setSearch} />
        </Gap>
        {filter(filter(pages, 'use'), search).map((page) => (
          <SideMenu.Item
            active={nav.path.indexOf(page) === 1}
            row
            key={page}
            label={page}
            link={`/${page}`}
            onClick={(e) => {
              e.preventDefault()
              nav.go(`/${page}`)
            }}
          />
        ))}
      </SideMenu>
    </Fit>
  )
}