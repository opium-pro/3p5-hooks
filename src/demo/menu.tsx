import React from 'react'
import { SideMenu } from 'opium-ui'
import * as Package from '../pkg'
import {useNav} from 'opium-nav'

const pages = Object.keys(Package)

export const Menu = () => {
  const nav = useNav()

  return (
    <SideMenu>
      {pages.map((page) => (
        <SideMenu.Item
          key={page}
          label={page}
          onClick={() => nav.go(`/${page}`)}
        />
      ))}
    </SideMenu>
  )
}
