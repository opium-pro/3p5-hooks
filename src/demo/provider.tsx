import Router from 'opium-nav'
import { Provider as RootProvider } from '../pkg'
import { UIProvider } from 'opium-ui'



export const Provider = ({ children }) => {
  return (
    <RootProvider>
      <UIProvider>
        <Router saveState={true}>
          {children}
        </Router>
      </UIProvider>
    </RootProvider>
  )
}
